var express = require('express')
var path = require('path')
var logger = require('morgan')
var config = require('./libs/config')
var bodyParser = require('body-parser')
var fs = require('fs')
var session = require('express-session')

var log = require('./libs/log')(module)
var LocationsModel = require('./libs/mongoose').LocationsModel
var PlayersModel = require('./libs/mongoose').PlayersModel
var UsersModel = require('./libs/mongoose').UserModel
var MessagesModel = require('./libs/mongoose').MessageModel

var app = express()
app.use(logger('dev'))
app.use(bodyParser({extended: false}))
app.use(session({secret: 'topsecret'}))

function loadUser(req, res, next) {
	if (req.session.user_id) {
		UserModel.findById(req.session.user_id, (err, user) => {
			if (user) {
				req.session.currentUser = user
				log.info("User found - %s", user.name)
				next()
			} else {
				log.err("User not found")
			}
		})
	} else if (req.originalUrl != '/') {
		res.redirect('/')
	} else {
		res.sendFile(__dirname + '/public/auth.html')
	}
}

app.get('/', loadUser, (req, res) => {
	
})

app.post('/', (req, res) => {
	console.log('Post')
	var body = req.body
	UsersModel.findOne({name: body.name}, (err, user) => {
		if (user && user.authenticate(body.password)) {
			req.session.user_id = user.id
			res.redirect('/player')
		} else {
			var User = new UsersModel ({
				name: body.name,
				password: body.password,
				playerName : ''
			})
			User.save((err) => {
				if (!err) {
					log.info("Игрок добавлен")
				} else {
					log.error("Ошибка при добавлении игрока")
					res.send({error: "Incorrect data"})
				}
			})
			req.session.user_id = User.id
			res.redirect('/players/add')
		}
	})
})

app.get('/logout', (req, res) => {
	req.session.destroy()
	res.redirect('/')
})

app.get('/player', (req, res) => {
	var userID = req.session.user_id
	console.log(req.session)
	if (userID) 
		UsersModel.findById(userID, (err, user) => {
			if (user.playerName != '') {
				res.redirect('players/' + user.playerName)
			} else {
				res.redirect('players/add')
			}
		})
	else
		res.redirect('/')
})

app.get('/locations', (req, res) => {
	return LocationsModel.find((err, locations) => {
		if (!err) {
			if (locations.length != 0) {
				var html = ""
				locations.forEach((location) => {
					html += "<a href = '/locations/" + location.locationID + "'>" + location.locationID + "<br>"
				})
				return res.send(html)
			}
			else
				return res.sendFile(__dirname + '/public/addLocation.html')
		} else {
			res.statusCode = 500
			log.error('Internal error(%d): %s', res.statusCode, err.message)
			return res.send({error: 'Server error'})
		}
	})
})

function addLoc(req, res) {
	var body = req.body
	var location = new LocationsModel({
		locationID: req.body.id,
		description: req.body.desc,
		type: req.body.type
	})

	location.save((err) => {
		if (!err) {
			log.info('Location saved')
			return res.send('Локация добавлена!')
		} else {
      log.error('Internal error(%d): %s',res.statusCode,err.message);
			if (err.name = 'ValidationError') {
				res.statusCode = 400
				res.send({error: "Неверные данные"})
			} else {
				res.statusCode = 500
				res.send({error: "Ошибка сервера"})
			}
		}
	})
}

app.post('/locations', addLoc)

app.get('/locations/add', (req, res) => {
	res.sendFile(__dirname + '/public/addLocation.html')
})

app.post('/locations/add', addLoc)

app.get('/locations/:id', (req, res) => {
	return LocationsModel.find({"locationID": req.params.id}, (err, location) => {
		if (!location) {
			res.statusCode = 404
			return res.send({error: 'Not found'})
		}
		if (!err) {
			var createLocPage = require('./public/addLocation.js')(location[0])
			return res.send(createLocPage)
			return res.send(location)
		} else {
			res.statusCode = 500
			log.error('Internal error(%d): %s', res.statusCode, err.message)
			return res.send({error: 'Server error'})
		}
	})
})

app.post('/locations/:id', (req, res) => {
	var body = req.body
	var location = {
		locationID: req.body.id,
		description: req.body.desc,
		type: req.body.type
	}

	LocationsModel.findOneAndUpdate({'locationID': req.body.id}, location, {new: true, upsert: true}, (err, _location) => {
		if (!err) {
			log.info('Location updated')
			return res.redirect('back')
		} else {
      log.error('Internal error(%d): %s', res.statusCode, err.message);
			if (err.name = 'ValidationError') {
				res.statusCode = 400
				res.send({error: "Неверные данные"})
			} else {
				res.statusCode = 500
				res.send({error: "Ошибка сервера"})
			}
		}
	})
})

app.get('/players', (req, res) => {
	return PlayersModel.find((err, players) => {
		if (!err) {
			if (players.length != 0) {
				var html = ""
				players.forEach((player) => {
					html += "<a href = '/players/" + player.name + "'>" + player.name + "<br>"
				})
				return res.send(html)
			} else
				return res.sendFile(__dirname + '/public/addPlayer.html')
		} else {
			res.statusCode = 500
			log.error('Internal error(%d): %s', res.statusCode, err.message)
			return res.send({error: 'Server error'})
		}
	})
})

app.get('/message', (req, res) => {
	var reciever = req.query.name
	var senderID = req.session.user_id
	console.log(reciever)
	PlayersModel.findOne({userid: senderID}, (err, player) => {
		var message = new MessagesModel({
			'sendID': player.name,
			'recieveID': reciever,
			'text': req.query.text
		})
		message.save((err) => {
			if (err) {
				log.error(err)
			} else {
				log.info('Message sent')
				res.redirect('/messages')
			}
		})
	})
})

app.get('/messages', (req, res) => {
	return MessagesModel.find((err, messages) => {
		if (!err) {
			var html = ""
			messages.forEach((message) => {
				html += message.sendID + ' -> ' + message.recieveID + ': ' + message.text + "<br>"
			})
			return res.send(html)
		} else {
			res.statusCode = 500
			log.error('Internal error(%d): %s', res.statusCode, err.message)
			return res.send({error: 'Server error'})
		}
	})
})

function addPlayer(req, res) {
	var body = req.body
	var player = new PlayersModel({
		name: req.body.name,
		class: req.body.class,
		email: req.body.email,
		level: req.body.level,
		position: req.body.pos,
		userid: req.session.user_id
	})

	player.save((err) => {
		if (!err) {
			var userID = req.session.user_id
			console.log(userID)
			UsersModel.update({'_id': userID}, {playerName: player.name}, (err) => {
				if (!err) {
					log.info('User linked to player')
					log.info('Player saved')
					return res.send('Игрок добавлен!')
				} else {
					log.error(err)
				}
			})
		} else {
      log.error('Internal error(%d): %s', res.statusCode, err.message);
			if (err.name = 'ValidationError') {
				res.statusCode = 400
				res.send({error: "Неверные данные"})
			} else {
				res.statusCode = 500
				res.send({error: "Ошибка сервера"})
			}
		}
	})
}

app.post('/players', addPlayer)

app.get('/players/add', (req, res) => {
	res.sendFile(__dirname + '/public/addPlayer.html')
})

app.post('/players/add', addPlayer)

app.get('/players/:name', (req, res) => {
	return PlayersModel.find({"name": req.params.name}, (err, player) => {
		if (!player) {
			res.statusCode = 404
			return res.send({error: 'Not found'})
		}
		if (!err) {
			var createCharPage = require('./public/addPlayer.js')(player[0])
			return res.send(createCharPage)
			return res.send(player)
		} else {
			res.statusCode = 500
			log.error('Internal error(%d): %s', res.statusCode, err.message)
			return res.send({error: 'Server error'})
		}
	})
})

app.post('/players/:name', (req, res) => {
	var body = req.body
	var player = {
		name: req.body.name,
		class: req.body.class,
		email: req.body.email,
		level: req.body.level,
		position: req.body.pos
	}

	PlayersModel.findOneAndUpdate({'name': req.body.name, 'userid': req.session.user_id}, player, {new: true, upsert: true}, (err, _player) => {
		if (!err) {
			log.info('Player updated')
			return res.redirect('back')
		} else {
      log.error('Internal error(%d): %s', res.statusCode, err.message);
			if (err.name = 'ValidationError') {
				res.statusCode = 400
				res.send({error: "Неверные данные"})
			} else {
				res.statusCode = 500
				res.send({error: "Ошибка сервера"})
			}
		}
	})
})

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

app.listen(config.get('port'), () => {
	log.info('Server is listening on port ' + config.get('port'))
})