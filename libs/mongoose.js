var mongoose = require('mongoose')
var log = require('./log')(module)
var config = require('./config')
var crypto = require('crypto')

mongoose.connect(config.get('mongoose:uri'))
var db = mongoose.connection

db.on('error', (err) => {
	log.error('Connection to db error: ', err.message)
})

db.once('open', () => {
	log.info('Connection to db: success')
})

var Schema = mongoose.Schema

var Message = new Schema({
	sendID: String,
	recieveID: String,
	text: String
})
var MessageModel = mongoose.model('messages', Message)

var Location = new Schema({
	locationID: {
		type: String,
		required: true
	},
	description: {
		type: String, 
		required: true
	},
	type: {
		type: String,
		enum: ['Forest', 'Desert', 'Dungeon', 'River', 'Ocean'],
		required: true
	}
})
var LocationsModel = mongoose.model('locations', Location)

var Player = new Schema({
	name: {
		type: String,
		required: true
	},
	class: {
		type: String,
		enum: ['Knight', 'Wizard', 'Thief', 'Paladin'],
		required: true
	},
	email: {
		type: String,
		required: true
	},
	level: {
		type: Number,
		required: true
	},
	position: {
		type: String,
		required: true
	},
	userid: String
})
var PlayersModel = mongoose.model('players', Player)

User = new Schema({
    'name': {
    	type: String,
    	required: true
    },
    'hashed_password': String,
    'salt': String,
    'playerName': String
  });

User.virtual('id')
  .get(function() {
    return this._id.toHexString();
  });

User.virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function() { return this._password; });

User.method('authenticate', function(plainText) {
  return this.encryptPassword(plainText) === this.hashed_password;
});

User.method('makeSalt', function() {
  return Math.round((new Date().valueOf() * Math.random())) + '';
});

User.method('encryptPassword', function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
});
var UserModel = mongoose.model('User', User);

module.exports.LocationsModel = LocationsModel
module.exports.PlayersModel = PlayersModel
module.exports.UserModel = UserModel
module.exports.MessageModel = MessageModel