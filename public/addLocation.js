function makeAddLocationPage(location) {
	var inputs = '';
	inputs += "Название<input type='text' name = 'id' value = '" + location.locationID + "'><br>"
	inputs += "Описание<input type='text' name = 'desc' value = '" + location.description + "'><br>Тип локации:<br>"

	if (location.type == 'Desert') {
		inputs += "<input type='radio' name = 'type' value = 'Desert' checked>Пустыня</input>"
	} else {
		inputs += "<input type='radio' name = 'type' value = 'Desert'>Пустыня</input>"
	}
	if (location.type == 'Forest') {
		inputs += "<input type='radio' name = 'type' value = 'Forest' checked>Лес</input>"
	} else {
		inputs += "<input type='radio' name = 'type' value = 'Forest'>Лес</input>"
	}
	if (location.type == 'Dungeon') {
		inputs += "<input type='radio' name = 'type' value = 'Dungeon' checked>Подземелье</input>"
	} else {
		inputs += "<input type='radio' name = 'type' value = 'Dungeon'>Подземелье</input>"
	}
	if (location.type == 'River') {
		inputs += "<input type='radio' name = 'type' value = 'River' checked>Река</input>"
	} else {
		inputs += "<input type='radio' name = 'type' value = 'River'>Река</input>"
	}
	if (location.type == 'Ocean') {
		inputs += "<input type='radio' name = 'type' value = 'Ocean' checked>Океан</input><br>"
	} else {
		inputs += "<input type='radio' name = 'type' value = 'Ocean'>Океан</input><br>"
	}
	
	var html = `<!DOCTYPE html>
			<html lang="en">
	 		<head>
	 			<meta charset="UTF-8">
	 			<title>Добавить локацию</title>
	 		</head>
			<body>
 	<form method = 'post'>` + inputs + `
 	<input type='submit' name="Добавить">
	 	</form>
	 </body>
	 </html>`
	 return html
}

module.exports = makeAddLocationPage
