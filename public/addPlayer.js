function makeAddPlayerPage(player) {
	var inputs = '';
	inputs += "Имя<input type='text' name = 'name' value = '" + player.name + "'><br>Класс:<br>"

	if (player.class == 'Knight') {
		inputs += "<input type='radio' name = 'class' value = 'Knight' checked>Рыцарь</input>"
	} else {
		inputs += "<input type='radio' name = 'class' value = 'Knight'>Рыцарь</input>"
	}
	if (player.class == 'Wizard') {
		inputs += "<input type='radio' name = 'class' value = 'Wizard' checked>Маг</input>"
	} else {
		inputs += "<input type='radio' name = 'class' value = 'Wizard'>Маг</input>"
	}
	if (player.class == 'Thief') {
		inputs += "<input type='radio' name = 'class' value = 'Thief' checked>Разбойник</input>"
	} else {
		inputs += "<input type='radio' name = 'class' value = 'Thief'>Разбойник</input>"
	}
	if (player.class == 'Paladin') {
		inputs += "<input type='radio' name = 'class' value = 'Paladin' checked>Паладин</input><br>"
	} else {
		inputs += "<input type='radio' name = 'class' value = 'Paladin'>Паладин</input><br>"
	}

	inputs += "email<input type='text' name = 'email' value = '" + player.email + "'><br>"
	inputs += "Уровень<input type='text' name = 'level' value = '" + player.level + "'><br>"
	inputs += "Локация<input type='text' name = 'pos' value = '" + player.position + "'><br>"

	
	var html = `<!DOCTYPE html>
	 <html lang='en'>
	 <head>
	 	<meta charset='UTF-8'>
	 	<title>Добавить игрока</title>
	 </head>
	 <body>
	 	<form method = 'post'>` + inputs + 	 		
	 		`<input type='submit' name="Добавить">
	 	</form>
	 </body>
	 </html>`
	 return html
}

module.exports = makeAddPlayerPage
